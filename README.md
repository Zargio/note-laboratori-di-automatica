# Una collezione di note e appunti per i laboratori di automatica
Abbiamo deciso di creare questa repo per aiutare gli altri del corso che potrebbero trovarsi non a loro agio con matlab, dandogli descrizioni più dettagliate di quelle che i prof hanno dato.
Puntiamo a rendere i file interattivi e come ottima fonte di ripetizione, non solo per prepararsi al progetto di automatica, ma perchè in quanto automatici usare questi strumenti è praticamente d'obbligo, ed è per questo che riteniamo sia essenziale la comprensione di queste tecnologie.
In futuro pianifichiamo di riscrivere il codice anche in linguaggi ~~veri~~ fatti apposta per queste cose, come python o julia, in modo tale che chiunque possa approfondire meglio queste tecnologie e stare al passo con le tecnologie di oggi.
Il tono dei file punta a essere molto rilassato e il meno formale possibile. Dopotutto questo è un progetto di studenti per studenti.

# Se avete idee, o qualcosa che non è chiaro **_DITECELO_**
Diteci di ogni idea per aggiungere o migliorare questa repo, così che sia il più comprensibile possibile. O anche se abbiamo scritto qualcosa di incorretto o typo. 

# Roadmap
- Scrivere introduzione brevissima a git (codeberg, questo sito su cui stai leggendo)
- Aggiungere link nel README
- Scrivere il lab0 intro a matlab, aggiungendo una spiegazione di live script e widget magari
- Completare il lab1 sulle sospensioni, migliorarlo, aggiungere qualche immagine
- Convertire i file in puro matlab per facilità di lettura sul sito
- Convertire i lab utilizzando [python](https://www.python.org/ "Homepage di python") con la libreria [numpy](https://numpy.org/ "Homepage di numpy") (date un'occhiata a [questo link](https://towardsdatascience.com/a-beginners-guide-to-simulating-dynamical-systems-with-python-a29bc27ad9b1) per avere un'idea di come funziona), [julia](https://julialang.org/ "Homepage di julia") e [jupyther](https://jupyter.org/ "Homepage di jupyter") aggiungendo spiegazioni sul motivo
